require('babel-polyfill');
var fs = require('fs');
var qs = require('qs');
var webpack = require('webpack');
var path = require('path');
var extractTextPlugin = require('extract-text-webpack-plugin');

var config = {
  devtool: 'cheap-module-eval-source-map',
  entry: {
    main: [
      // configuration for babel6
      'babel-polyfill',
      './src/scripts/main.js',
      './src/styles/main.scss'
    ]
  },
  output: {
    path: path.join(__dirname, '/dist/'),
    publicPath: '/wp-content/themes/choices/dist/',
    filename: 'scripts/main.js',
  },
  module: {
    loaders: [{
      test: /\.js$/,
      loader: 'eslint-loader',
      include: __dirname + '/src/',
      enforce: 'pre'
    },
    {
      test: /\.scss$/,
      include: path.join(__dirname, 'src', 'styles'),
      use: extractTextPlugin.extract({
        fallback: 'style-loader',
        use: ['css-loader', 'sass-loader']
      })
    }]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new webpack.DefinePlugin({
      __DEVELOPMENT__: true
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.ProgressPlugin(function(percentage, msg) {
      console.log((percentage * 100) + '%', msg);
    }),
    new extractTextPlugin('styles/[name].css')
  ],
  resolve: {
    // Allow to omit extensions when requiring these files
    extensions: [".js", "scss"],
  }
}

module.exports = config;
